from itertools import permutations, product
from pprint import pprint
from math import ceil

team_points = {
    'Secret' :4710,
    'PSG.LGD':4071,
    'Mineski':3150,
    'Newbee' :2220,
    'VG'     :2160,
    'VGJ.T'  :1935,
    'EG'     :1335,
}
tournaments = (
    (('Secret', 'EG'),
     (150, 90)),
    (('Mineski', 'Newbee', 'Secret', 'VG', 'PSG.LGD'),
     (750, 450, 225, 75)),
    (('Newbee', 'EG', 'VG', 'Mineski'),
     (750, 450, 225, 75)),
    (('Secret', 'Newbee', 'VG', 'VGJ.T', 'EG', 'Mineski', 'PSG.LGD'),
     (1125, 675, 337.5, 112.5)),
)

tour_ranks = []
for teams, points in tournaments:
    tour_ranks.append([])
    for rank in permutations(teams, len(points)):
        tour_ranks[-1].append(tuple(zip(rank, points)))

max_9th_points = 0
closest_tour_res = None
closest_team_points = None
count = 0
for tour_res in product(*tour_ranks):
    count += 1
    if count % 100000 == 0:
        print(count, end='\r')
    # Accumuate points (3x)
    temp = team_points.copy()
    for tour in tour_res:
        for t, p in tour:
            temp[t] += 3 * p
    # Find and record second min points
    min_pts = min(temp.items(), key=lambda x:x[1])[1]
    if max_9th_points < min_pts:
        max_9th_points = min_pts
        closest_tour_res = tour_res
        closest_team_points = temp

print(count)
max_needed_points = ceil(max_9th_points + 0.5)
print('Max needed points = {}'.format(ceil(max_9th_points + 0.5)))
pprint(closest_tour_res)
pprint(sorted(closest_team_points.items(), key=lambda x:-x[1]))
